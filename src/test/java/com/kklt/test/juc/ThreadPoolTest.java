package com.kklt.test.juc;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.junit.Test;

import java.util.concurrent.*;

public class ThreadPoolTest {

    @Test
    public void test1() throws InterruptedException {
        ThreadFactory threadFactory = new ThreadFactoryBuilder()
                .setNameFormat("文件上传-%d")
                .build();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(4,
                8,
                4,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(100),
                threadFactory,
                new ThreadPoolExecutor.DiscardPolicy());
        System.out.println(threadPoolExecutor.getPoolSize());
        // 执行108个任务,使得工作队列已经满的时候,核心线程不够用,创建到最大线程数量8
        for (int i = 0; i < 108; i++) {
            threadPoolExecutor.execute(() -> {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        // 刚执行108个任务后线程池线程数量
        Thread.sleep(1090);
        System.out.println(threadPoolExecutor.getPoolSize());

        // 执行完任务超过空闲时间后线程池线程的数量
        Thread.sleep(4000);
        System.out.println(threadPoolExecutor.getPoolSize());
    }

    /**
     * 执行任务,不需要等待线程执行的结果
     */
    @Test
    public void test2(){
        ThreadFactory threadFactory = new ThreadFactoryBuilder()
                .setNameFormat("test2-%d")
                .build();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(4,
                8,
                4,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(100),
                threadFactory,
                new ThreadPoolExecutor.DiscardPolicy());

        threadPoolExecutor.execute(()->{
            System.out.println("thread execute ....");
        });
    }

    /**
     * 执行任务,需要在异步任务处理完成之后进行某些操作
     */
    @Test
    public void test3(){
        ThreadFactory threadFactory = new ThreadFactoryBuilder()
                .setNameFormat("test3-%d")
                .build();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(4,
                8,
                4,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(100),
                threadFactory,
                new ThreadPoolExecutor.DiscardPolicy());

        Future<String> submit = threadPoolExecutor.submit(() -> {
            System.out.println("thread execute do something one....");
            return "task complete can do something";
        });

        // do something two

        try {
            // 等待异步任务完成.
            String result = submit.get();
            // do something
            System.out.println(result);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * 关闭线程池.等待正在执行和工作队列中的任务执行完成.
     */
    @Test
    public void test4(){
        ThreadFactory threadFactory = new ThreadFactoryBuilder()
                .setNameFormat("test4-%d")
                .build();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(4,
                8,
                4,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(100),
                threadFactory,
                new ThreadPoolExecutor.DiscardPolicy());

        threadPoolExecutor.execute(()->{
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("thread execute ....");
        });

        threadPoolExecutor.shutdown();
        try {
            Thread.sleep(2100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * 关闭线程池.等待正在执行的任务完成和抛弃工作队列中的任务.
     */
    @Test
    public void test5(){
        ThreadFactory threadFactory = new ThreadFactoryBuilder()
                .setNameFormat("test5-%d")
                .build();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(4,
                8,
                4,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(100),
                threadFactory,
                new ThreadPoolExecutor.DiscardPolicy());

        threadPoolExecutor.execute(()->{
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("thread execute ....");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("thread execute ....");
        });

        threadPoolExecutor.shutdownNow();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 常用的监控线程池的方法
     */
    @Test
    public void test6() throws InterruptedException {
        ThreadFactory threadFactory = new ThreadFactoryBuilder()
                .setNameFormat("test6-%d")
                .build();
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(4,
                8,
                4,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(100),
                threadFactory,
                new ThreadPoolExecutor.DiscardPolicy());
        System.out.println("线程池中线程数量:"+threadPoolExecutor.getPoolSize());
        System.out.println("已经执行和未执行任务数量:"+threadPoolExecutor.getTaskCount());
        System.out.println("执行完成任务数量:"+threadPoolExecutor.getCompletedTaskCount());
        System.out.println("正在执行任务线程数量:"+threadPoolExecutor.getActiveCount());
        System.out.println("--------------------------------------------------------");
        // 执行108个任务,使得工作队列已经满的时候,核心线程不够用,创建到最大线程数量8
        for (int i = 0; i < 108; i++) {
            threadPoolExecutor.execute(() -> {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        System.out.println("线程池中线程数量:"+threadPoolExecutor.getPoolSize());
        System.out.println("已经执行和未执行任务数量:"+threadPoolExecutor.getTaskCount());
        System.out.println("执行完成任务数量:"+threadPoolExecutor.getCompletedTaskCount());
        System.out.println("正在执行任务线程数量:"+threadPoolExecutor.getActiveCount());
        System.out.println("--------------------------------------------------------");
        // 刚执行108个任务后线程池线程数量
        Thread.sleep(1090);
        System.out.println("线程池中线程数量:"+threadPoolExecutor.getPoolSize());
        System.out.println("已经执行和未执行任务数量:"+threadPoolExecutor.getTaskCount());
        System.out.println("执行完成任务数量:"+threadPoolExecutor.getCompletedTaskCount());
        System.out.println("正在执行任务线程数量:"+threadPoolExecutor.getActiveCount());
        System.out.println("--------------------------------------------------------");

        // 执行完任务超过空闲时间后线程池线程的数量
        Thread.sleep(4000);
        System.out.println("线程池中线程数量:"+threadPoolExecutor.getPoolSize());
        System.out.println("已经执行和未执行任务数量:"+threadPoolExecutor.getTaskCount());
        System.out.println("执行完成任务数量:"+threadPoolExecutor.getCompletedTaskCount());
        System.out.println("正在执行任务线程数量:"+threadPoolExecutor.getActiveCount());
        System.out.println("--------------------------------------------------------");
    }

    @Test
    public void test7(){
        Executors.newSingleThreadExecutor();
        Executors.newCachedThreadPool();
        Executors.newFixedThreadPool(4);
        Executors.newScheduledThreadPool(4);
    }
}
