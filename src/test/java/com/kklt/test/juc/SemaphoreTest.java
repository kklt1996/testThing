package com.kklt.test.juc;

import org.junit.Test;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * 信号量测试
 */
public class SemaphoreTest {

    @Test
    public void test1() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        // 设置信号量为2
        Semaphore semaphore = new Semaphore(2);

        // 并发执行4次任务
        for (int i = 0; i < 4; i++) {
            executorService.execute(() -> {
                try {
                    System.out.println("task start time:"+new Date());
                    semaphore.acquire();
                    // do something
                    Thread.sleep(1000);
                    // do something
                    System.out.println("task end time:"+new Date());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    semaphore.release();
                }
            });
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test2() {
        ExecutorService executorService = Executors.newCachedThreadPool();
        // 连接池数量设置为5,超时时间是6s
        DataBaseConnPool dataBaseConnPool = new DataBaseConnPool(5, 6000L);
        // 并发执行4次任务
        for (int i = 0; i < 36; i++) {
            executorService.execute(() -> {
                // 获取连接
                Connection connection = dataBaseConnPool.getConnection();
                // 执行sql
                connection.runSql("select name from tbl_user");
                // 关闭连接
                connection.closeConnection();
            });
        }
        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }




    public class DataBaseConnPool{
        // 数据库链接最大连接数
        private Integer maxPoolSize;
        // 获取数据库连接的超时时间.
        private Long timeOut;

        // 信号量
        private Semaphore semaphore;

        public DataBaseConnPool(Integer maxPoolSize, Long timeOut) {
            this.maxPoolSize = maxPoolSize;
            this.timeOut = timeOut;
            semaphore = new Semaphore(maxPoolSize);
        }

        /**
         * 获取数据库连接
         * @return
         */
        public Connection getConnection(){
            boolean getConn = false;
            try {
                getConn = semaphore.tryAcquire(timeOut, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (getConn){
                return new Connection(semaphore);
            }else {
                throw new RuntimeException("获取数据库链接超时!");
            }
        }

    }

    /**
     * 数据库连接
     */
    public class Connection{
        // 信号量
        private Semaphore semaphore;

        public Connection(Semaphore semaphore) {
            this.semaphore = semaphore;
        }

        /**
         * 执行sql
         * @param sql
         */
        public void runSql(String sql){
            try {
                Thread.sleep(1000);
                System.out.println(new Date()+" exec sql:"+sql+" getWaitQueueLength: "+ semaphore.getQueueLength() +" availablePool: "+semaphore.availablePermits());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        /**
         * 关闭数据库连接
         */
        public void closeConnection(){
            semaphore.release();
        }
    }
}
