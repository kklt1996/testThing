package com.kklt.test.juc;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class AtomicTest {

    private ExecutorService executorService;

    @Before
    public void init(){
        executorService = Executors.newCachedThreadPool();
    }

    @Test
    public void atomicIntegerTest(){
        AtomicInteger atomicInteger = new AtomicInteger(0);
        int countTime = 100;
        CountDownLatch countDownLatch = new CountDownLatch(countTime);
        for (int i = 0; i < countTime; i++) {
            executorService.execute(()->{
                atomicInteger.incrementAndGet();
                countDownLatch.countDown();
            });
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("count:"+atomicInteger.get());
    }

    @Test
    public void atomicBooleanTest(){
        AtomicBoolean atomicBoolean = new AtomicBoolean();
        for (int i = 0; i < 10; i++) {
            executorService.execute(()->{
                boolean setFlag = atomicBoolean.compareAndSet(false, true);
                System.out.println(setFlag);
            });
        }
    }


}
