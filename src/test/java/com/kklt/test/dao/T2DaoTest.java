package com.kklt.test.dao;


import com.kklt.test.entity.T2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.LinkedList;
import java.util.List;


@SpringBootTest
@RunWith(SpringRunner.class)
public class T2DaoTest {

    @Autowired
    private T2Dao dao;

    @Test
    public void contextLoads() {
    }

    @Test
    public void TestBatchInsert1(){
        T2 record1 = new T2();
        record1.setA("test1");
        T2 record2 = new T2();
        record2.setA("test2");
        List<T2> list = new LinkedList<>();
        list.add(record1);
        list.add(record2);
        dao.batchInsert(list);
        System.out.println("record1:"+record1);
        System.out.println("record2:"+record2);
    }
}
