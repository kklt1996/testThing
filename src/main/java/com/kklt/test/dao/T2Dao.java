package com.kklt.test.dao;

import com.kklt.test.entity.T2;

import java.util.List;

public interface T2Dao {
    int deleteByPrimaryKey(Integer id);

    int insert(T2 record);

    int batchInsert(List<T2> records);

    int insertSelective(T2 record);

    T2 selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(T2 record);

    int updateByPrimaryKey(T2 record);
}