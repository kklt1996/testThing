package com.kklt.test.entity;

import java.io.Serializable;
import lombok.Data;

/**
 * t2
 * @author 
 */
@Data
public class T2 implements Serializable {
    private Integer id;

    private String a;

    private static final long serialVersionUID = 1L;
}